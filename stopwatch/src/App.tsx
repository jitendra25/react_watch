import React from 'react';
import '../src/App.css';
import Watch from './components/Watch';
interface Props{

}
interface State{

}
class App extends React.Component<Props,State>{
  constructor(props:Props){
    super(props);
  }
  render(){
    return(
      <div className={"cont"}>
        <h3>React Stopwatch</h3>
        <Watch />
      </div>
    );
  }
}
export default App;