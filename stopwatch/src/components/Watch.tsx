import React from 'react';
import '../components/Watch.css'
interface Props {}
interface State {
   
    min :number,
    sec:number,
    msec:number,
    btn:string
}
class  Watch extends React.Component<Props,State> {
    id:any = 0;
    constructor(props:Props) {
        super(props);
        this.state={
            min:0,
            sec:0,
            msec:0,
            btn:"Start"
        };
      
    }
    reset = ()=>{
        clearInterval(this.id)
        this.setState({
           min:0,
           sec:0,
           msec:0,
           btn:"Start"
        })
    }
    startt = ()=>{
      if(this.state.btn==="Pause"){
       clearInterval(this.id);
      this.setState({btn:"Start"});
      }
      else{
           this.setState({ btn: "Pause" });
           this.id = setInterval(() => {
             let mse = this.state.msec;
             let secc = this.state.sec;
             let ss: number = 0,
               m = 0;
             if (mse === 99) {
               ss = 1;
               console.log("1");
             }
             if(secc===60){
                this.setState((state, props) => ({
                  sec: 0,
                  min:state.min + 1,
                }));
             }
             this.setState((state, props) => ({
               sec: state.sec + ss,
               msec: Math.floor((state.msec + 1) % 100),
             }));
           }, 10);
      }
    }
    render(){
        return(
            <div className={"main"}>
           <p>{this.state.min} : {this.state.sec} : {this.state.msec}</p>
           <div className={"btn"}>
               <button onClick={this.startt}>{this.state.btn}</button>
               <button onClick={this.reset}>Reset</button>
           </div>
            </div>
        )
    }
};
export default Watch;