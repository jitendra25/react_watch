import { clear } from "console";
import React from "react";
import "./Count.css";

interface Props {
  time: number;
}
interface State {
  totalSec: number;
  day: number;
  hour: number;
  min: number;
  sec: number;
}
class Count extends React.Component<Props, State> {
  tim:any;
  constructor(props: Props) {
    super(props);
    const secc = props.time;
    let dayy = Math.floor(secc / (24 * 3600));
    let houry = Math.floor((secc % (24 * 3600)) / 3600);
    let miny = Math.floor(((secc % (24 * 3600)) % 3600) / 60);
    let secy = Math.floor(((secc % (24 * 3600)) % 3600) % 60);
    this.state = {
      totalSec: secc,
      day: dayy === 0 ? -1 : dayy,
      hour: houry === 0 ? -1 : houry,
      min: miny === 0 ? -1 : miny,
      sec: secy === 0 ? -1 : secy,
    };
    this.clear = this.clear.bind(this);
  }
  componentDidMount() {
     this.tim = setInterval(() => {
      const secc = this.state.totalSec;
      if (secc === 0) {
        alert("completed");
        clearInterval(this.tim);
      }
      this.setState((state, props) => ({
        totalSec: secc - 1,
        day: state.day !== -1 ? Math.floor(secc / (24 * 3600)) : -1,
        hour: state.hour !== -1 ? Math.floor((secc % (24 * 3600)) / 3600) : -1,
        min:
          state.min !== -1
            ? Math.floor(((secc % (24 * 3600)) % 3600) / 60)
            : -1,
        sec:
          state.sec !== -1
            ? Math.floor(((secc % (24 * 3600)) % 3600) % 60)
            : -1,
      }));
    }, 1000);
  }
  clear(){
    clearInterval(this.tim);
    this.setState({
       totalSec:0,
       day:0,
       hour:0,
       min:0,
       sec:0
    });
  }
  render() {
    return (
      <div>
        <div className="nav">
          <p>Countdown Timer</p>
          <button onClick={this.clear}>Clear</button>
        </div>
        <div>
          <div className={"cont"}>
            <h3>Countdown ends in...</h3>
            <div className={"main"}>
              {this.state.day !== -1 && (
                <div>
                  <span className={"text"}>{this.state.day}</span>
                  <br />
                  <span className={"tag"}>Days</span>
                </div>
              )}
              {this.state.hour !== -1 && (
                <div>
                  <span className={"text"}>{this.state.hour}</span>
                  <br />
                  <span className={"tag"}>Hours</span>
                </div>
              )}
              {this.state.min !== -1 && (
                <div>
                  <span className={"text"}>{this.state.min}</span>
                  <br />
                  <span className={"tag"}>Mins</span>
                </div>
              )}
              {this.state.sec !== -1 && (
                <div>
                  <span className={"text"}>{this.state.sec}</span>
                  <br />
                  <span className={"tag"}>Secs</span>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Count;
